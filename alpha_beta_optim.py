# Player Alan_Turing

import random as rnd
import numpy as np
from datetime import datetime

class GameState(object):
    __slots__ = ['board', 'playerToMove', 'winner']

# Global variables
boardWidth = 0
boardHeight = 0
homeWidth = 0
homeHeight = 0
timeLimit = 0
isTimeOut = False
deadEnd= False
hasWinnerEnd = False

# Compute list of legal moves for a given GameState for the player moving next 
def getMoveOptions(state):
    direction = [[(1, 0), (0, 1), (2, 2)], [(-1, 0), (0, -1), (-2, -2)]]              # Possible (dx, dy) moving directions for each player
    checkNeigbhorDirection = [[(1, 0), (0, 1), (1, 1)], [(-1, 0), (0, -1), (-1, -1)]]
    moves = []
    hasOpponent = False
    for xStart in range(boardWidth):                                # Search board for player's pieces
        for yStart in range(boardHeight):
            if state.board[xStart, yStart] == state.playerToMove:   # Found a piece!
                for (dx, dy) in direction[state.playerToMove]:      # Check horizontal and vertical moving directions
                    (xEnd, yEnd) = (xStart + dx, yStart + dy)
                    while xEnd >= 0 and xEnd < boardWidth and yEnd >= 0 and yEnd < boardHeight:
                        if (dx == 2 and dy == 2) or (dx == -2 and dy == -2):
                            if state.board[xEnd, yEnd] == 1 - state.playerToMove :
                                hasOpponent = True
                            break
                        if state.board[xEnd, yEnd] == -1:
                            moves.append((xStart, yStart, xEnd, yEnd))      # If square is free, we have a legal move...
                            break
                            
                        else:
                            if state.board[xEnd, yEnd] == 1 - state.playerToMove :
                                    hasOpponent = True
                        xEnd += dx                                          # Otherwise, check for larger step
                        yEnd += dy
    return (hasOpponent, moves)

# For a given GameState and move to be executed, return the GameState that results from the move
def makeMove(state, move):
    (xStart, yStart, xEnd, yEnd) = move
    newState = GameState()

    newState.playerToMove = 1 - state.playerToMove          # After the move, it's the other player's turn
    newState.board = np.copy(state.board)
    newState.board[xStart, yStart] = -1                     # Remove the piece at the start position
    if (state.playerToMove == 0 and (xEnd < boardWidth - homeWidth or yEnd < boardHeight - homeHeight)) or \
       (state.playerToMove == 1 and (xEnd >= homeWidth or yEnd >= homeHeight)):
        newState.board[xEnd, yEnd] = state.playerToMove     # Unless the move ends in the opponent's home, place piece at end position
    
    for xStart in range(boardWidth):
        for yStart in range(boardHeight):
            if newState.board[xStart, yStart] == state.playerToMove:
                newState.winner = -1                        # If the player still has pieces on the board, then there is no winner yet...
                return newState
    
    newState.winner = state.playerToMove                    # Otherwise, the current player has won!
    return newState

# Return the evaluation score for a given GameState; higher score indicates a better situation for Player 1
# Alan_Turing's evaluation function is based on the (non-jump) moves each player would need to win the game. 
def getScore(state):
    score = 0
    player1Score = 0
    player2Score = 0
    for x in range(boardWidth):                             # Search board for any pieces
        for y in range(boardHeight):
            if state.board[x, y] == 0:                      # Subtract the number of moves (non-jumps) for Player 1's piece to reach Player 2's home area
                # jump = checkNeighbor(state, x, y)
                player1Score += max([0, boardWidth - homeWidth - x]) + max([0, boardHeight - homeHeight - y])

            else:
                if state.board[x, y] == 1:                  # Add the number of moves (non-jumps) for Player 2's piece to reach Player 1's home area
                    # jump = checkNeighbor(state, x, y)
                    player2Score += max([0, x - homeWidth+1]) + max([0, y - homeHeight+1])
    score -= player1Score
    score += player2Score
    if player1Score == 0 or player2Score == 0:
        return [score, True]
    else:
        return [score, False]

# Check whether time limit has been reached
def timeOut(startTime, timeLimit):
    duration = datetime.now() - startTime
    return duration.seconds + duration.microseconds * 1e-6 >= timeLimit

# Compute the next move to be played; keep updating <bestMoveSoFar> until computation finished or time limit reached
def getMove(state, hWidth, hHeight, timeLimit):
    # Set global variables
    global boardWidth, boardHeight, homeWidth, homeHeight, bestMoveSoFar, isTimeOut, deadEnd
    boardWidth = state.board.shape[0]
    boardHeight = state.board.shape[1]
    homeWidth = hWidth
    homeHeight = hHeight
    startTime = datetime.now()
    bestMoveSoFar = []
    myMove = state.playerToMove
    if state.playerToMove == 0:
        depth = 1
        isTimeOut = False
        deadEnd = False
        hasWinnerEnd = False
        while True:
            move = getMaxValue(startTime, timeLimit, state, 1, depth, [-9999, []], -9999, 9999)[1] 
            if isTimeOut:
                #print ("time out")
                return bestMoveSoFar
            if deadEnd or hasWinnerEnd:
                #print ("end by no neighbor")
                return move
            bestMoveSoFar = move
            #print ("done loop")
            depth = depth + 1
    else :
        depth = 1
        isTimeOut = False
        deadEnd = False 
        hasWinnerEnd = False
        while True:
            move = getMinValue(startTime, timeLimit, state, 1, depth, [9999, []], -9999, 9999)[1] 
            if isTimeOut:
                #print ("time out")
                return bestMoveSoFar
            if deadEnd or hasWinnerEnd :
                #print ("end by no neighbor")
                return move
            bestMoveSoFar = move
            depth = depth + 1
            # print(bestMoveSoFar)
    

def getMaxValue(startTime, timeLimit, state, startDepth, depth, currentValue, alpha, beta):

    # if time out return 
    global isTimeOut, deadEnd, hasWinnerEnd
    if timeOut(startTime, timeLimit):
        isTimeOut = True
        return currentValue

    # if reach the highest depth return
    if startDepth > depth : 
        return currentValue

    # do max min checking
    hasOpponent, moveList = getMoveOptions(state)
    minValue = [-float('inf'), currentValue[1]]

    # if deadEnd :
    #     for move in moveList :
    #         projectedState = makeMove(state, move)
    #         minValue[0] = min(minValue[0], getScore(projectedState)[0])
    #     return minValue
    for move in moveList:
        projectedState = makeMove(state, move)
        score = getScore(projectedState)
        currentValue = [score[0], move]
        # print(hasOpponent)
        tmp = []
        # print(score[1], hasOpponent)
        if score[1] :
            #print ("hasWinner ")
            hasWinnerEnd = True
            tmp = currentValue
        elif (not hasOpponent and startDepth == 1) or deadEnd:
            deadEnd = True
            tmp = getMinValue(startTime, timeLimit, projectedState, startDepth+1, max(homeHeight, homeWidth), currentValue, alpha, beta)
        else :
            tmp = getMinValue(startTime, timeLimit, projectedState, startDepth+1, depth, currentValue, alpha, beta)

        if minValue[0] < tmp[0]:
            minValue[0] = tmp[0]
            if startDepth == 1:
                # print("should go here", depth)
                minValue[1] = tmp[1]   
        if alpha < minValue[0]:
            alpha = minValue[0]

        if alpha >= beta:
            break
        # print ("loop")
    return minValue
        
        
def getMinValue(startTime, timeLimit, state, startDepth, depth, currentValue, alpha, beta):

    # if time out return 
    global isTimeOut, deadEnd, hasWinnerEnd
    if timeOut(startTime, timeLimit):
        isTimeOut = True
        return currentValue

    # if reach the highest depth return
    if startDepth > depth :
        return currentValue

    # do max min checking
    maxValue = [float('inf'), currentValue[1]]
    hasOpponent, moveList = getMoveOptions(state)

    for move in moveList:
        projectedState = makeMove(state, move)
        score = getScore(projectedState)
        currentValue = [score[0], move]
        tmp = []
        if score[1] :
            #print ("hasWinner ")
            hasWinnerEnd = True
            tmp = currentValue
        elif (not hasOpponent and startDepth == 1) or deadEnd:
            deadEnd = True
            tmp = getMaxValue(startTime, timeLimit, projectedState, startDepth+1,  max(homeHeight, homeWidth), currentValue, alpha, beta)
        else :
            tmp = getMaxValue(startTime, timeLimit, projectedState, startDepth+1, depth, currentValue, alpha, beta)
        # print (tmp)
        if maxValue[0] > tmp[0] :
            maxValue[0] = tmp[0]
            if startDepth == 1:
                # print("should go here", depth)
                maxValue[1] = tmp[1]
        if beta > maxValue[0] :
            beta = maxValue[0]

        if beta <= alpha and startDepth != 1:
            break
        # print ("loop")
    return maxValue
