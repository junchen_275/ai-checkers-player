import importlib
import numpy as np
import random as rnd
import time
from datetime import datetime

class GameState(object):
    __slots__ = ['board', 'playerToMove', 'winner']

# Global variables
boardWidth = 6
boardHeight = 6
homeWidth = 2
homeHeight = 2
timeLimit = 1

state = GameState()     # Create initial game state
state.board = -np.ones((boardWidth, boardHeight), dtype=int)
state.board[:homeWidth, :homeHeight] = 0
state.board[-homeWidth:, -homeHeight:] = 1
state.playerToMove = 0
state.winner = -1
#print(state.board.shape[0])
#print(state.board.shape[1])

# Compute list of legal moves for a given GameState for the player moving next 
def getMoveOptions(state):
    direction = [[(1, 0), (0, 1)], [(-1, 0), (0, -1)]]              # Possible (dx, dy) moving directions for each player
    moves = []
    for xStart in range(boardWidth):                                # Search board for player's pieces
        for yStart in range(boardHeight):
            if state.board[xStart, yStart] == state.playerToMove:   # Found a piece!
                for (dx, dy) in direction[state.playerToMove]:      # Check horizontal and vertical moving directions
                    (xEnd, yEnd) = (xStart + dx, yStart + dy)
                    while xEnd >= 0 and xEnd < boardWidth and yEnd >= 0 and yEnd < boardHeight:
                        if state.board[xEnd, yEnd] == -1:
                            moves.append((xStart, yStart, xEnd, yEnd))      # If square is free, we have a legal move...
                            break
                        xEnd += dx                                          # Otherwise, check for larger step
                        yEnd += dy
    newMoves = sorted(moves, key=straightL) #key=lambda x: straightL(x)
    return newMoves

def straightL(move):
    length = np.sqrt(np.square(move[2] - (boardWidth - 1)) + np.square(move[3] - (boardHeight - 1)))
    return length

def makeMove(state, move):
    (xStart, yStart, xEnd, yEnd) = move
    newState = GameState()

    newState.playerToMove = 1 - state.playerToMove          # After the move, it's the other player's turn
    newState.board = np.copy(state.board)
    newState.board[xStart, yStart] = -1                     # Remove the piece at the start position
    if (state.playerToMove == 0 and (xEnd < boardWidth - homeWidth or yEnd < boardHeight - homeHeight)) or \
       (state.playerToMove == 1 and (xEnd >= homeWidth or yEnd >= homeHeight)):
        newState.board[xEnd, yEnd] = state.playerToMove     # Unless the move ends in the opponent's home, place piece at end position
    
    for xStart in range(boardWidth):
        for yStart in range(boardHeight):
            if newState.board[xStart, yStart] == state.playerToMove:
                newState.winner = -1                        # If the player still has pieces on the board, then there is no winner yet...
                return newState
    
    newState.winner = state.playerToMove                    # Otherwise, the current player has won!
    return newState

def getScore(state):
    score = 0
    for x in range(boardWidth): 
        for y in range(boardHeight):
            if state.board[x, y] == 0:
                jumpX = 0
                for n in range(1,(boardWidth - homeWidth - x)):
                    if state.board[x + n, y] != -1:
                        jumpX = jumpX + 1
                    else:
                        break
                jumpY = 0
                for n in range(1, boardHeight - homeHeight - y):
                    if state.board[x, y + n] != -1:
                        jumpY = jumpY + 1
                    else:
                        break
                if jumpX >= jumpY:
                    score -= max([0, boardWidth - homeWidth - x - jumpX]) + max([0, boardHeight - homeHeight - y])
                else:
                    score -= max([0, boardWidth - homeWidth - x]) + max([0, boardHeight - homeHeight - y - jumpY])
                print("p1", score)
            elif state.board[x, y] == 1:
                jumpX = 0
                for n in range(1, x - homeWidth + 1):
                    if state.board[x - n, y] != -1:
                        jumpX = jumpX + 1
                    else:
                        break
                jumpY = 0
                for n in range(1, y - homeHeight + 1):
                    if state.board[x, y - n] != -1:
                        jumpY = jumpY + 1
                    else:
                        break
                if jumpX >= jumpY:
                    score += max([0, x - homeWidth + 1 - jumpX]) + max([0, y - homeHeight + 1])
                else:
                    score += max([0, x - homeWidth + 1]) + max([0, y - homeHeight + 1 - jumpY])
                print("p2", score)
    return score

def timeOut(startTime, timeLimit):
    duration = datetime.now() - startTime
    return duration.seconds + duration.microseconds * 1e-6 >= timeLimit

def getMove(state, hWidth, hHeight, timeLimit):
    # Set global variables
    global boardWidth, boardHeight, homeWidth, homeHeight, bestMoveSoFar
    boardWidth = state.board.shape[0]
    boardHeight = state.board.shape[1]
    homeWidth = hWidth
    homeHeight = hHeight
    
    startTime = datetime.now()                                       # Remember computation start time

    moveList = getMoveOptions(state)                                 # Get the list of possible moves
    bestMoveSoFar = moveList[0]                                      # Just choose first move from the list for now, in case we run out of time 
    scoreList = []
    for move in moveList:
        projectedState = makeMove(state, move)                       # For each move, play it on a separate board...
        scoreList.append(getScore(projectedState))                   # ... and call the evaluation function on the resulting GameState
    
        if timeOut(startTime, timeLimit):                            # Check for timeout and return current best move if time limit is reached
            return bestMoveSoFar                                     # It is not necessary for Alan_Turing, but you need to include this check in
    print(scoreList)                                                                 # all time-consuming loops in your code so that you will not exceed the time limit
    if state.playerToMove == 0:                                      # Finally, pick the move with the best score
        bestMoveSoFar = moveList[scoreList.index(max(scoreList))]    # If we are Player 1, we look for the maximum score
    else:
        bestMoveSoFar = moveList[scoreList.index(min(scoreList))]    # If we are Player 2, we look for the minimum score
    
    return bestMoveSoFar

m = getMoveOptions(state)
print(m)
newState0 = makeMove(state, (0, 0, 2, 0))
print(newState0.board)
#getScore(newState0)
m0 = getMoveOptions(newState0)
#print(m0)
newState1 = makeMove(newState0, (4, 4, 3, 4))
print(newState1.board)
x = getScore(newState1)
print(x)

"""
newState1 = makeMove(newState0, (4, 4, 3, 4))
print(newState1.board)

newState2 = makeMove(newState1, (0, 1, 0, 0))
print(newState2.board)

newState3 = makeMove(newState2, (4, 5, 4, 4))
print(newState3.board)
m3 = getMoveOptions(newState3)
print(m3)
score1 = getScore(newState3)
print(score1)

for x in range(boardWidth):                             # Search board for any pieces
    for y in range(boardHeight):
        if newState3.board[x, y] == 0:
            print([0, boardWidth - homeWidth - x])
            #print([0, boardHeight - homeHeight - y])

for x in range(boardWidth):                             # Search board for any pieces
    for y in range(boardHeight):
        if newState3.board[x, y] == 0:
            print([0, boardHeight - homeHeight - y])
"""