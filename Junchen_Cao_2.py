# Player Junchen_Cao_2

import random as rnd
import numpy as np
from datetime import datetime
import math

class GameState(object):
    __slots__ = ['board', 'playerToMove', 'winner']

# Global variables
boardWidth = 6
boardHeight = 6
homeWidth = 2
homeHeight = 3
timeLimit = 1


# Compute list of legal moves for a given GameState for the player moving next 
def getMoveOptions(state):
    direction = [[(1, 0), (0, 1)], [(-1, 0), (0, -1)]]              # Possible (dx, dy) moving directions for each player
    moves = []
    for xStart in range(boardWidth):                                # Search board for player's pieces
        for yStart in range(boardHeight):
            if state.board[xStart, yStart] == state.playerToMove:   # Found a piece!
                for (dx, dy) in direction[state.playerToMove]:      # Check horizontal and vertical moving directions
                    (xEnd, yEnd) = (xStart + dx, yStart + dy)
                    while xEnd >= 0 and xEnd < boardWidth and yEnd >= 0 and yEnd < boardHeight:
                        if state.board[xEnd, yEnd] == -1:
                            moves.append((xStart, yStart, xEnd, yEnd))      # If square is free, we have a legal move...
                            break
                        xEnd += dx                                          # Otherwise, check for larger step
                        yEnd += dy
    if state.playerToMove == 0:
        newMoves = sorted(moves, key=straightL0)
    else:
        newMoves = sorted(moves, key=straightL1)    
    return newMoves

def straightL0(move):
    #length = np.sqrt(np.square(move[0] - move[2]) + np.square(move[0] - move[2]))
    length = np.sqrt(np.square(move[2] - boardWidth) + np.square(move[3] - boardHeight))
    return length

def straightL1(move):
    length = np.sqrt(np.square(move[2] - 0) + np.square(move[3] - 0))
    return length


# For a given GameState and move to be executed, return the GameState that results from the move
def makeMove(state, move):
    (xStart, yStart, xEnd, yEnd) = move
    newState = GameState()

    newState.playerToMove = 1 - state.playerToMove          # After the move, it's the other player's turn
    newState.board = np.copy(state.board)
    newState.board[xStart, yStart] = -1                     # Remove the piece at the start position
    if (state.playerToMove == 0 and (xEnd < boardWidth - homeWidth or yEnd < boardHeight - homeHeight)) or \
       (state.playerToMove == 1 and (xEnd >= homeWidth or yEnd >= homeHeight)):
        newState.board[xEnd, yEnd] = state.playerToMove     # Unless the move ends in the opponent's home, place piece at end position
    
    for xStart in range(boardWidth):
        for yStart in range(boardHeight):
            if newState.board[xStart, yStart] == state.playerToMove:
                newState.winner = -1                        # If the player still has pieces on the board, then there is no winner yet...
                return newState
    
    newState.winner = state.playerToMove                    # Otherwise, the current player has won!
    return newState

# Return the evaluation score for a given GameState; higher score indicates a better situation for Player 1
# Alan_Turing's evaluation function is based on the (non-jump) moves each player would need to win the game. 
def getScore0(state):
    score = 0
    for x in range(boardWidth): 
        for y in range(boardHeight):
            if state.board[x, y] == 0:
                jumpX = 0
                for n in range(1,(boardWidth - homeWidth - x)):
                    if state.board[x + n, y] == 0:
                        jumpX = jumpX + 1
                    else:
                        break
                jumpY = 0
                for n in range(1, boardHeight - homeHeight - y):
                    if state.board[x, y + n] == 0:
                        jumpY = jumpY + 1
                    else:
                        break
                if jumpX >= jumpY:
                    score -= max([0, boardWidth - homeWidth - x - jumpX]) + max([0, boardHeight - homeHeight - y])
                else:
                    score -= max([0, boardWidth - homeWidth - x]) + max([0, boardHeight - homeHeight - y - jumpY])
            elif state.board[x, y] == 1:
                jumpX = 0
                for n in range(1, x - homeWidth + 1):
                    if state.board[x - n, y] == 1:
                        jumpX = jumpX + 1
                    else:
                        break
                jumpY = 0
                for n in range(1, y - homeHeight + 1):
                    if state.board[x, y - n] == 1:
                        jumpY = jumpY + 1
                    else:
                        break
                if jumpX >= jumpY:
                    score += max([0, x - homeWidth + 1 - jumpX]) + max([0, y - homeHeight + 1])
                else:
                    score += max([0, x - homeWidth + 1]) + max([0, y - homeHeight + 1 - jumpY])
    return score


def getScore(state):
    score = 0
    for x in range(boardWidth):                             # Search board for any pieces
        for y in range(boardHeight):
            if state.board[x, y] == 0:                      # Subtract the number of moves (non-jumps) for Player 1's piece to reach Player 2's home area
                score -= max([0, boardWidth - homeWidth - x]) + max([0, boardHeight - homeHeight - y])
            else:
                if state.board[x, y] == 1:                  # Add the number of moves (non-jumps) for Player 2's piece to reach Player 1's home area
                    score += max([0, x - homeWidth + 1]) + max([0, y - homeHeight + 1])
    return score

# Check whether time limit has been reached
def timeOut(startTime, timeLimit):
    duration = datetime.now() - startTime
    return duration.seconds + duration.microseconds * 1e-6 >= timeLimit

# Compute the next move to be played; keep updating <bestMoveSoFar> until computation finished or time limit reached
def getMove(state, hWidth, hHeight, timeLimit):
    startTime = datetime.now()
    duration = 0
    moveList = getMoveOptions(state)
    scoreList = [None] * len(moveList)
    bestMoveSoFar = moveList[0]
    depth = 3
    while duration < timeLimit:
        i = 0
        for move in moveList:
            #print(move)
            projectedState = makeMove(state, move)
            #j = scoreList[i]                       
            scoreList[i] = (minimaxPruning(projectedState, depth, -np.inf, np.inf, startTime, timeLimit))
            #k = scoreList[i]
            #print(j, k)
            if timeOut(startTime, timeLimit):
                break
            i = i + 1
        #print(scoreList)
        #print(scoreList.index(max(scoreList)))
        if state.playerToMove == 0:
            bestMoveSoFar = moveList[scoreList.index(max(scoreList))]
        else:
            bestMoveSoFar = moveList[scoreList.index(min(scoreList))]
        depth = depth + 1
        duration = ((datetime.now() - startTime).seconds + (datetime.now() - startTime).microseconds * 1e-6)
        #print('Depth %d completed after %.4f seconds'%(depth, duration))
        if duration >= timeLimit:
                break
    return bestMoveSoFar

 
def minimaxPruning(state, depth, alpha, beta, startTime, timeLimit):
    if timeOut(startTime, timeLimit):
        return getScore(state)
        #(getScore0(state) + getScore(state))/2
    if state.winner == 0:
        score = 10000
        return score
    elif state.winner == 1:
        score = -10000
        return score
    if depth == 0:
        return getScore(state)
        #(getScore0(state) + getScore(state))/2
    moveList = getMoveOptions(state)
    if state.playerToMove == 0:
        maxBest = -np.inf
        for move in moveList:
            projectedState = makeMove(state, move)
            score = minimaxPruning(projectedState, (depth - 1), alpha, beta, startTime, timeLimit)
            maxBest = max(maxBest, score)
            alpha = max(alpha, score)
            if beta <= alpha:
                break
        return maxBest
    elif state.playerToMove == 1:
        minBest= np.inf
        for move in moveList:
            projectedState = makeMove(state, move)
            score = minimaxPruning(projectedState, (depth - 1), alpha, beta, startTime, timeLimit)  
            minBest = min(minBest, score)
            beta = min(beta, score)
            if beta <= alpha:
                break
        return minBest

def minimax(state, depth):
    if state.winner == 0:
        score = 100
        return score
    elif state.winner == 1:
        score = -100
        return score

    if depth == 0:
        return getScore(state)
    moveList = getMoveOptions(state)
    if state.playerToMove == 0:
        bestScore = -np.inf
        for move in moveList:
            projectedState = makeMove(state, move)
            score = minimax(projectedState, (depth - 1))
            #print(score)
            bestScore = max(bestScore, score)
        return bestScore
    elif state.playerToMove == 1:
        bestScore = np.inf
        for move in moveList:
            projectedState = makeMove(state, move)
            score = minimax(projectedState, (depth - 1))  
            bestScore = min(bestScore, score)
        return bestScore